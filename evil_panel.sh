#!/bin/bash

source /home/marulo/.rpvar

checkbar=$(pgrep dzen2 | wc -l)
if [ "$checkbar" != "0" ]; then
	killall dzen2

else
	evil_status.sh | dzen2 -ta l -h 25 -bg $rpcolor_0 -fg $rpcolor_0 -fn 'hack nerd font-16' &

    fi
