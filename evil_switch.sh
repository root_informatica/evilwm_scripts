#!/bin/bash

WINDOW=$(wmctrl -l -x | tr '.' ' ' | awk '{print $1,$2,$3,$6}' | dmenu -i -l 8 -p window | cut -d ' ' -f 1-2)
CW=$(wmctrl -d | grep '*' | cut -c 1)

echo "$WINDOW" > /tmp/evil_switch

window=$(cat /tmp/evil_switch | awk '{print $1}')
workspace=$(cat /tmp/evil_switch | awk '{print $2}')

  if [ "$CW" != "$workspace" ]; then
      wmctrl -s $workspace && xdotool windowraise $window

  else
      xdotool windowraise $window 

  fi
