#!/bin/bash

OPTIONS=" h\n l\n k\n j\n y\n u\n b\n n"
LAUNCHER="dmenu -i -l 8"

option=`echo -e $OPTIONS | $LAUNCHER | awk '{print $2}'`
if [ ${#option} -gt 0 ]
then
    case $option in
      h)
	  xdotool getwindowfocus windowmove 16 16 && xdotool getwindowfocus windowsize 610 768
        ;;
      l)
          xdotool getwindowfocus windowmove 645 16 && xdotool getwindowfocus windowsize 618 768
        ;;
      k)
          xdotool getwindowfocus windowmove 16 16 && xdotool getwindowfocus windowsize 1248 405
        ;;
      j)
          xdotool getwindowfocus windowmove 16 436 && xdotool getwindowfocus windowsize 1248 348
        ;;
      y)
	  xdotool getwindowfocus windowmove 16 16 && xdotool getwindowfocus windowsize 610 405
	  ;;
      u)
	  xdotool getwindowfocus windowmove 645 16 && xdotool getwindowfocus windowsize 618 405
	  ;;
      b)
	  xdotool getwindowfocus windowmove 16 436 && xdotool getwindowfocus windowsize 610 348
	  ;;
      n)
	  xdotool getwindowfocus windowmove 645 436 && xdotool getwindowfocus windowsize 618 348
	  ;;
      *)
        ;;
    esac

    fi

##               ##
