#!/bin/bash

IN=(`echo | dmenu -l 8`)

notification() {
    sleep $IN && pkill -SIGUSR1 herbe & herbe " - timer ($IN) finished!"
    mplayer /home/marulo/.tareas.mp3 > /dev/null
}


  if [ -n "$IN" ]; then
      xterm -g 46X6 -e termdown -b -f slant
      
  else
      xterm -g 46X6 -e termdown -b -f slant -t " - timer finished!" $IN &
      sleep 2; $(notification)

  fi
    
#     祥  
