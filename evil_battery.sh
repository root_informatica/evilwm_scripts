#!/bin/bash

source /home/marulo/.rpvar

STATE=$(acpi | awk '{print $3}')
PERCENT=$(acpi | awk '{print $4}' | cut -d '%' -f 1)
BAR0="^fg()^r(2x16)^fg($rpcolor_0)^r(4x16)"
BAR1="^fg($rpcolor_1)^r(2x16)^fg($rpcolor_0)^r(4x16)"
BAR2="^fg($rpcolor_5)^r(2x16)^fg($rpcolor_0)^r(4x16)"
BAR3="^fg($rpcolor_6)^r(2x16)^fg($rpcolor_0)^r(4x16)"
BAR4="^fg($rpcolor_6)^r(2x16)^fg($rpcolor_0)^r(4x16)"

charge() {
    if [ "$PERCENT" == "100" ]; then
	echo "$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1"
	
    elif [ "$PERCENT" -ge "90" ]; then
	echo "$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR0"

    elif [ "$PERCENT" -ge "80" ]; then
	echo "$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR0$BAR0"

    elif [ "$PERCENT" -ge "70" ]; then
	echo "$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "60" ]; then
	echo "$BAR1$BAR1$BAR1$BAR1$BAR1$BAR1$BAR0$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "50" ]; then
	echo "$BAR1$BAR1$BAR1$BAR1$BAR1$BAR0$BAR0$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "40" ]; then
	echo "$BAR1$BAR1$BAR1$BAR1$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "30" ]; then
	echo "$BAR1$BAR1$BAR1$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "20" ]; then
	echo "$BAR1$BAR1$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "10" ]; then
	echo "$BAR1$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "0" ]; then
	echo "$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0"
	
    fi
}


discharge() {
    if [ "$PERCENT" == "100" ]; then
	echo "$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2"

    elif [ "$PERCENT" -ge "90" ]; then
	echo "$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2$BAR0"

    elif [ "$PERCENT" -ge "80" ]; then
	echo "$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2$BAR2$BAR0$BAR0"

    elif [ "$PERCENT" -ge "70" ]; then
	echo "$BAR3$BAR3$BAR3$BAR3$BAR3$BAR3$BAR3$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "60" ]; then
	echo "$BAR3$BAR3$BAR3$BAR3$BAR3$BAR3$BAR0$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "50" ]; then
	echo "$BAR3$BAR3$BAR3$BAR3$BAR3$BAR0$BAR0$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "40" ]; then
	echo "$BAR3$BAR3$BAR3$BAR3$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "30" ]; then
	echo "$BAR4$BAR4$BAR4$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "20" ]; then
	echo "$BAR4$BAR4$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0"

    elif [ "$PERCENT" -ge "10" ]; then
	echo "$BAR4$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0$BAR0"
     
    fi
}

    if [ "$STATE" == "Charging," ] || [ "$STATE" == "Full," ]; then

    echo "^fg($rpcolor_2)B^fg() $(charge)^fg()"

else

    echo "^fg($rpcolor_2)B^fg() $(discharge)^fg()"

    fi
