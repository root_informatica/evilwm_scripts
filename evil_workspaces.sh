#!/bin/bash

source /home/marulo/.rpvar

checkbar=$(pgrep dzen2 | wc -l)
 if [ "$checkbar" != "0" ]; then
     killall dzen2

     else


name() {
 NAME=($({ wmctrl -l | awk '{print "+ "$2}' & wmctrl -d | awk '{print $2,$1}';} | tr -d ' ' | sort | uniq -c -s 1 | tr -d ' ' | cut -c 2-3 | tr '*' '>')) 
 for i in "${NAME[@]}"; do
     echo -n " "
     case ${i:0:1} in
        '>')
            echo -n "^fg($rpcolor_1)${i:1} ^fg($rpcolor_3)"
            ;;
	'+')
	    echo -n "^fg($rpcolor_2)${i:1} ^fg($rpcolor_3)"
	    ;;
        *)
            echo -n "${i:1} "
             ;;
     esac
    echo -n "^ca()"

 done

}

## output ##
while true; do
    echo "$(name)"
    sleep 1

done | dzen2 -fn 'hack nerd font-14' -fg $rpcolor_3 -bg '#000000' &

 fi
