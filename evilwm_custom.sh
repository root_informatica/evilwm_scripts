#!/bin/bash

/home/marulo/.screenlayout/comodore_default.sh &

# xsetroot -solid '#000000'

 ~/.fehbg &

 pulsemixer --unmute &

 check_charge.sh &
 
 xautolock.sh &

 weather_gen.sh &

 xrdb -merge .Xresources &

 sxhkd &

 exec evilwm
 
