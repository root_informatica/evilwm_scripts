#!/bin/bash

OPTIONS="-hibernate\n-power-off\n-reboot\n-logout"

LAUNCHER="dmenu -i -l 8"

HIBERNATE="sudo pm-hibernate"

REBOOT="sudo shutdown -r now"

POWER_OFF="sudo shutdown -h now"

EXIT_SESSION="evil_exit.sh"


option=`echo -e $OPTIONS | $LAUNCHER | awk '{print $1}' | tr -d '\r\n'`
if [ ${#option} -gt 0 ]
then
    case $option in
      -hibernate)
      $HIBERNATE
        ;;
      -power-off)
        $POWER_OFF
        ;;
      -reboot)
        $REBOOT
        ;;
      -logout)
        $EXIT_SESSION
        ;;
      *)
        ;;
    esac
fi
