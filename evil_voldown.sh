#!/bin/bash

amixer -D pulse sset Master 10%-

vol=$(amixer sget Master | grep 'Front Left:' | awk '{print $5}' | sed 's/\[//g;s/\]//g;s/\%//g')

 if [ "$vol" -ge "100" ]; then
     pkill -SIGUSR1 herbe & herbe  " volume ========== 100%"

 elif [ "$vol" -ge "90" ]; then
    pkill -SIGUSR1 herbe & herbe  " volume =========- 90%"
 
 elif [ "$vol" -ge "80" ]; then
     pkill -SIGUSR1 herbe & herbe  " volume ========-- 80%"

 elif [ "$vol" -ge "70" ]; then
     pkill -SIGUSR1 herbe & herbe  " volume =======--- 70%"

 elif [ "$vol" -ge "60" ]; then
      pkill -SIGUSR1 herbe & herbe  " volume ======---- 60%"

 elif [ "$vol" -ge "50" ]; then
      pkill -SIGUSR1 herbe & herbe  " volume =====----- 50%"

 elif [ "$vol" -ge "40" ]; then
      pkill -SIGUSR1 herbe & herbe  " volume ====------ 40%"

 elif [ "$vol" -ge "30" ]; then
      pkill -SIGUSR1 herbe & herbe  " volume ===------- 30%"

 elif [ "$vol" -ge "20" ]; then
      pkill -SIGUSR1 herbe & herbe  " volume ==-------- 20%"

 elif [ "$vol" -ge "10" ]; then
      pkill -SIGUSR1 herbe & herbe  " volume =--------- 10%"

 elif [ "$vol" -ge "0" ]; then
      pkill -SIGUSR1 herbe & herbe  " volume ---------- 0%"

      fi
