#!/bin/bash

WINDOWS=$(wmctrl -l | wc -l)

if [ "$WINDOWS" == "0" ]; then
    kill $(pgrep -f evil_status.sh)
    sleep 1;
    kill $(pgrep -f sxhkd)
    killall evilwm

else
    pkill -SIGUSR1 herbe & herbe -c "Windows remain open"
    exit 0

          fi
